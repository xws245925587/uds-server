/*
 * udsServer_utils.c
 *
 *  Created on: Apr 25, 2021
 *      Author: taoyu
 */

#include "uds_utils.h"

uint16_t uds_getWordFromArray(uint8_t * buffer)
{
	uint16_t data = 0;

	/// MSB first
	data = buffer[0];
	data <<= 8;
	data |= buffer[1];

	return data;
}

int uds_putByteToArray(uint8_t data, uint8_t * buffer)
{
	buffer[0] = data;
	return 1;
}

int uds_putWordToArray(uint16_t data, uint8_t * buffer)
{
	/// MSB First
	buffer[1] = (uint8_t)data;
	data >>= 8;
	buffer[0] = (uint8_t)data;
	return 2;
}

