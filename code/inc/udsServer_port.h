/*
 * udsServer_port.h
 *
 *  Created on: Apr 25, 2021
 *      Author: taoyu
 */

#ifndef UDSSERVER_INC_UDSSERVER_PORT_H_
#define UDSSERVER_INC_UDSSERVER_PORT_H_

#include <stdint.h>
#include "uds_types.h"

// IO Control descriptor
typedef struct
{
	uint16_t did;
	UDS_NRC (*ioControl)(uint16_t did, UDS_IOCP iocp, uint8_t * pData, uint8_t length);
}
UDS_IOC_DESC;

// read by id descriptor
typedef struct
{
	uint16_t did;
	UDS_NRC (*readDataByID)(uint16_t did, uint8_t * pData, uint8_t bufferSize, uint8_t * pDataLength);
}
UDS_RDBI_DESC;

// write by id descriptor
typedef struct
{
	uint16_t did;
	UDS_NRC (*writeDataByID)(uint16_t did, uint8_t * buffer, int length);
}
UDS_WDBI_DESC;

// routine control descriptor
typedef struct
{
	uint16_t rid;
	UDS_NRC (*routineControlStart)(uint16_t rid, uint8_t * pData, uint8_t reqLen, uint8_t * rspLen);
	UDS_NRC (*routineControlStop)(uint16_t rid, uint8_t * pData, uint8_t reqLen, uint8_t * rspLen);
	UDS_NRC (*routineControlRequestResult)(uint16_t rid, uint8_t * pData, uint8_t reqLen, uint8_t * rspLen);
}
UDS_RC_DESC;

extern void udsServer_pport_init(void);

extern void udsServer_pport_task1ms(void);

extern int udsServer_rport_send(uint8_t * buffer, int length);

extern int udsServer_rport_receive(uint8_t * buffer, int length);

#endif /* UDSSERVER_INC_UDSSERVER_PORT_H_ */

