/*
 * uds_utils.h
 *
 *  Created on: Apr 25, 2021
 *      Author: taoyu
 */

#ifndef UDSSERVER_INC_UDS_UTILS_H_
#define UDSSERVER_INC_UDS_UTILS_H_

#include <stdint.h>

extern uint16_t uds_getWordFromArray(uint8_t * buffer);

extern int uds_putByteToArray(uint8_t data, uint8_t * buffer);
extern int uds_putWordToArray(uint16_t data, uint8_t * buffer);

#endif /* UDSSERVER_INC_UDS_UTILS_H_ */
